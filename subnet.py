class DataMismatch(Exception):
    def __init__(self, message="Invalid data provided."):
        self.message = message
        super().__init__(self.message)

def generate_subnets(address, subnets_count):
    octets = address.split("/")[0].split(".")
    mask = int(address.split("/")[1])
    bits_required = required_bits(subnets_count)
    hosts_count = hosts_in_subnet(mask)
    new_mask = mask + bits_required
    hosts_per_subnet = int(hosts_count / subnets_count)
    if new_mask > 30:
        return("Zu viele Subnetze!")
    else:
        return compute_subnets(address.split("/")[0], hosts_per_subnet, subnets_count, new_mask)

def compute_subnets(subnet, hosts_per_network, subnets_count, new_mask):
    subnet_binary = "".join([f'{int(x):08b}'  for x in subnet.split(".")])
    subnet_decimal = int(subnet_binary, 2)
    result = []
    
    for i in  range(0, subnets_count):
        network_decimal = subnet_decimal + (hosts_per_network * i)
        network_binary = bin(network_decimal)[2:].zfill(32)
        network_octets = [network_binary[i:i+8] for i in range(0, 32, 8)]
        network_address = ".".join(str(int(octet, 2)) for octet in network_octets)
        result.append(f"{network_address}/{new_mask}")
    return result

def hosts_in_subnet(mask): 
    return 2 ** (32 - mask)

def new_mask(subnet_bits, mask):
    return subnet_bits + mask

def required_bits(subnets):
    return (subnets - 1).bit_length()

if __name__ == '__main__':
    while True:
        network_address = input("Bitte IP Adresse im gewünschten Format eingeben (z.B. 192.168.4.0/24): ")
        subnets_count = int(input("Bitte Anzahl der Subnetze eingeben: "))
        try:
            result = generate_subnets(network_address, subnets_count)
            print(result)
        except DataMismatch as e:
            print(e.message)